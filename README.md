# ◇─ HIVE™ VPS MANAGER ─◇

ㅤ
<p align="left">
  <a href="" rel="noopener">
 <img src="https://gitlab.com/mrvybes/HIVE-VPS-CONFIG/-/raw/a04ae02fb82412d080492328a5d35c44b8c3c78a/Install/vps_logo.png" alt="logo"></a>
</p>


## ◇─ SETUP ─◇

___Update System Packages and Install SCRIPT___

```
apt-get update -y; apt-get upgrade -y; wget https://gitlab.com/mrvybes/HIVE-VPS-CONFIG/-/raw/1ce98988b76abadf8fc5566daf0ba7dbb2cb3a92/hive; chmod 777 hive;./hive

```  
## ◇─ ⚠️ Important Note ⚠️ ─◇

<p><i>The main source is owned <a href="https://t.me/s/singledevelopers/">@SingleDevelopers</a> not mine.<i></p>

 
##  ㅤ

___Licence___

[![License](https://www.gnu.org/graphics/gplv3-127x51.png)](LICENSE)

